package com.example.duoc.flaviorocha_prueba1;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.duoc.flaviorocha_prueba1.entidades.Usuario;

import java.util.ArrayList;

import static android.R.attr.checked;

public class LoginActivity extends AppCompatActivity {
    private ArrayList<Usuario> dataSource;
    private static ArrayList<Usuario> arrayStaticoUsuario = BaseDeDatos.obtieneListadoUsuarios();

    private EditText etUsuario;
    private EditText etPassword;
    private Button btnEntrar;
    private Button btnRegistrarse;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        etUsuario = (EditText) findViewById(R.id.etUsuario);
        etPassword = (EditText) findViewById(R.id.etPassword);

        btnEntrar = (Button) findViewById(R.id.btnEntrar);
        btnRegistrarse = (Button) findViewById(R.id.btnRegistrarse);
        //POBLAR BD
        poblarBaseDeDatos();
        //ON CLICK DE REGISTRARSE
        btnRegistrarse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(LoginActivity.this, RegistroActivity.class);
                startActivity(i);
            }
        });
        //ON CLICK DE ENTRAR
        btnEntrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(validarUsuario() == 1) {
                    Intent i = new Intent(LoginActivity.this, ListadoUsuariosActivity.class);
                    i.putExtra("nombreUsuario", etUsuario.getText().toString());
                    startActivity(i);
                    etUsuario.setText("");
                    etPassword.setText("");
                    etUsuario.requestFocus();
                } else {
                    Toast.makeText(LoginActivity.this, "Usuario y/o Clave incorrectas.", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void poblarBaseDeDatos() {
        //Usuario 1
        Usuario u1 = new Usuario();
        u1.setUsuario("fl.rocha");
        u1.setPassword("gusnjqzi");
        BaseDeDatos.agregarUsuario(u1);
        //Usuario 2
        Usuario u2 = new Usuario();
        u2.setUsuario("administrador");
        u2.setPassword("javq2j3x");
        BaseDeDatos.agregarUsuario(u2);
        //Usuario 3
        Usuario u3 = new Usuario();
        u3.setUsuario("fe.faune");
        u3.setPassword("cerveza");
        BaseDeDatos.agregarUsuario(u3);
        //Usuario 4
        Usuario u4 = new Usuario();
        u4.setUsuario("king.hassan");
        u4.setPassword("fatego");
        BaseDeDatos.agregarUsuario(u4);
    }

    private int validarUsuario() {
        ArrayList<Usuario> listUsuarios = getDataSource();
        int respuesta = 0;

        Usuario usuario = new Usuario();
        usuario.setUsuario(etUsuario.getText().toString());
        usuario.setPassword(etPassword.getText().toString());

        for (int i = 0; i < listUsuarios.size(); i++) {
            if (usuario.getUsuario().equals(listUsuarios.get(i).getUsuario()) && usuario.getPassword().equals(listUsuarios.get(i).getPassword())) {
                respuesta = 1;
            }
        }
        return respuesta;
    }

    public ArrayList<Usuario> getDataSource() {

        if (dataSource == null) {
            dataSource = new ArrayList<>();

            for (int x = 0; x < arrayStaticoUsuario.size(); x++) {
                Usuario u = new Usuario(arrayStaticoUsuario.get(x).getUsuario(), arrayStaticoUsuario.get(x).getPassword());
                dataSource.add(u);
            }
        }
        return dataSource;
    }
}


