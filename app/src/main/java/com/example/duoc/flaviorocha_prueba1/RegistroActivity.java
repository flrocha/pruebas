package com.example.duoc.flaviorocha_prueba1;

import android.app.Activity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.duoc.flaviorocha_prueba1.entidades.Usuario;

public class RegistroActivity extends AppCompatActivity {
    private EditText etUsuarioRegistro;
    private EditText etPasswordRegistro;
    private EditText etRePassword;
    private Button btnCrear;
    private Button btnVolver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro);
        //et
        etUsuarioRegistro = (EditText) findViewById(R.id.etUsuarioRegistro);
        etPasswordRegistro = (EditText) findViewById(R.id.etPasswordRegistro);
        etRePassword = (EditText) findViewById(R.id.etRePassword);
        //btn
        btnCrear = (Button) findViewById(R.id.btnCrear);
        btnVolver = (Button) findViewById(R.id.btnVolver);
        //click btnVolver
        btnVolver.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        //click en btnCrear
        btnCrear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //pregunto por espacios vacios
                if (etUsuarioRegistro.getText().toString().trim().equals("") ||
                        etPasswordRegistro.getText().toString().trim().equals("") ||
                        etRePassword.getText().toString().trim().equals("")) {
                    //si estan vacios manda toast
                    Toast.makeText(RegistroActivity.this, "Todos los campos son requeridos.", Toast.LENGTH_SHORT).show();
                } else {
                    if (etPasswordRegistro.getText().toString().equals(etRePassword.getText().toString())) {
                        //guardo el usuario
                        guardarUsuario();
                        //limpio los campos y doy focus
                        etUsuarioRegistro.setText("");
                        etPasswordRegistro.setText("");
                        etRePassword.setText("");
                        etUsuarioRegistro.requestFocus();
                    } else {
                        //claves no iguales manda toast
                        Toast.makeText(RegistroActivity.this, "Las Claves introducidas deven ser iguales.", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });

    }

    private void guardarUsuario() {
        Usuario usuario = new Usuario();

        usuario.setUsuario(etUsuarioRegistro.getText().toString());
        usuario.setPassword(etPasswordRegistro.getText().toString());

        BaseDeDatos.agregarUsuario(usuario);
        Toast.makeText(this, "Usuario registrado correctamente", Toast.LENGTH_SHORT).show();
        //Toast.makeText(this, etUsuarioRegistro.getText().toString() + " " + etPasswordRegistro.getText().toString() , Toast.LENGTH_SHORT).show();
    }
}
